# QEMU- IPROUTE2

## VUE D'ENSEMBLE

Ce mode opératoire explique comment créer un pont qui contient au moins un périphérique ethernet. Ceci est utile pour des choses comme le mode pont de QEMU, la mise en place d'un point d'accès logiciel, etc. Nous verrons deux façons de configurer un réseau dans un environnement virtuel, avec l'outil iproute2. La commande bctl sera quant à elle écartée, car elle parait être obsolète. 

## OBJECTIFS


- Créer le réseau à l’aide des commandes Iproute2

1. Création du bridge
2. Configuration de la VM
3. Configuration des interfaces



## ENVIRONNEMENT 

![](qemu-ovs.png)

## COMMANDES

- Créer le réseau à l’aide des commandes Iproute2

1. Création du bridge

L’idée ici, est de configurer un bridge (br0) entre la carte enx00e04c534458 et les interfaces où sont connectées les machines virtuelles (tap0 et tap1) afin que le trafic puisse transiter jusqu’à la passerelle par défaut (qui est ici la box internet) qui distribue les IP, de façon transparente.

Création du bridge :

```bash
ip link add br0 type bridge
```
Et l’activer :
```bash
ip link set br0 up
```
Pour ajouter une interface (par exemple eth0) dans le pont, son état doit être actif :
```bash
ip link set enx00e04c534458 up
```
L'ajout de l'interface au bridge se fait en définissant le bridge comme master :
```bash
ip link set enx00e04c534458 master br0
```
2. Configuration de la VM

Installation de qemu :

```bash
sudo apt install qemu qemu-utils qemu-system-x86
```
Création d’une image :

```bash
qemu-img create -f qcow2 -o preallocation=metadata ./virtual_machine.qcow2 10G
```
Puis lancement de l’image d’installation :

```bash
qemu-system-x86_64 -smp 2 -m 4096 -enable-kvm \
  -cdrom CentOS-8.5.2111-x86_64-boot.iso -boot d \
  -netdev user,id=n1 -device virtio-net-pci,netdev=n1  -device virtio-scsi-pci -drive file=./virtual_machine.qcow2,if=none,id=net1,format=qcow2,cache=none -device scsi-hd,drive=net1

```

3. Configuration des interfaces

Installation des outils pour manipuler les connexions au bridge :
```bash
apt install uml-utilities bridge-utils
```
Création du script dans /tmp/demo.sh :

```bash
#!/bin/bash
switch='br0'
/sbin/ip link set up dev $1
/sbin/ip link set $1 master ${switch}
exit 0

```
```bash
#!/bin/bash
switch='br0'
/sbin/ip link set down dev $1
/sbin/ip link set $1 nomaster
exit 0
```
```bash
sudo chmod +x /tmp/demo*
```
Lancement de la VM :

```bash
sudo qemu-system-x86_64 -smp 2 -m 4096 -vnc 0.0.0.0:1 -enable-kvm -monitor stdio -k fr -net tap,script=/tmp/demo-up.sh -net nic virtual_machine.qcow2
```

On peut voir ici le bridge créé (avec la connexion tap0):

```bash
sudo ip link show br0
```
```bash
bridge name    bridge id        STP enabled    interfaces
br0        8000.7e5513d8e35d    no        enx00e04c534458
                            tap0
```







